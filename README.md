# Demandware Code Styleguide #

This styleguide is meant to help the community by applying a defined set of standards to the development process.

## [Frontend Styleguide](codestyle-client.md)

**Contributions welcome!**

## [Serverside Styleguide](codestyle-server.md)

## Setup studio for linting

*TBC*