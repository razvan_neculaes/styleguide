# CSS formatting
CSS files are formatted in single-line style where each class definition is placed in one line.
Multiple definitions belonging to the same element should be grouped together where multiple groups of definitions are separated by a blank line.

Example:

```
    /* recommendations */
    .recommendations .details{margin:0 0 2px 0; color:#FF0029}
    .recommendations a{text-decoration:underline}
    .recommendationLink a:hover{text-decoration:none}

    /* recommendations in layer */
    .layerPopup .recommendations2nd{border-top:0}
    .layerPopup .recommendations .col{width:188px; height:147px;}
```
